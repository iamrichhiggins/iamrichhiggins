module.exports = function (grunt) {

	// 1. All configuration goes here 
	grunt.initConfig({
		criticalcss: {
			home: {
				options: {
					url: "http://localhost:4000/",
	                width: 1200,
	                height: 900,
	                outputfile: "_includes/_css/home.css",
	                filename: "_site/css/main.css", // Using path.resolve( path.join( ... ) ) is a good idea here
	                buffer: 800*1024,
	                ignoreConsole: false
				}
			},
			projects: {
				options: {
					url: "http://localhost:4000/projects/",
	                width: 1200,
	                height: 900,
	                outputfile: "_includes/_css/projects.css",
	                filename: "_site/css/main.css", // Using path.resolve( path.join( ... ) ) is a good idea here
	                buffer: 800*1024,
	                ignoreConsole: false
				}
			}			
		},
		concat: {   
		    dist: {
		        src: [
					'bower_components/fastclick/lib/fastclick.js',
					'bower_components/jquery/dist/jquery.min.js',
		            'js/custom_libs/bootstrap.min.js'	//	http://getbootstrap.com/customize/?id=4ddb1e7845fd036a4d1c
		        ],
		        dest: 'js/libs.js'
		    }
		},
		grunticon: {
		    icons: {
		        files: [{
		            expand: true,
		            cwd: 'img/icons',
		            src: ['*.svg', '*.png'],
		            dest: 'img/grunticon'
		        }],
		        options: {
					enhanceSVG:true,
					corsEmbed:true
		        }
		    }
		}
	});
	
	// 3. Where we tell Grunt we plan to use this plug-in.
	grunt.loadNpmTasks('grunt-criticalcss');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-grunticon');
	
	// 4. Where we tell Grunt what to do when we into the terminal.
	grunt.registerTask('libs', ['concat']);
	grunt.registerTask('icon', ['grunticon:icons']);
	grunt.registerTask('ccss', ['criticalcss:home','criticalcss:projects']);
}