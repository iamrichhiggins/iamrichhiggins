---
layout: post
title:  "Ch-ch-ch-ch-changes"
date:   2016-02-05 10:36:00 +0000
categories: jekyll update
---
It's been an awful long time since I had <a href="https://web.archive.org/web/20060703040147/http://www.lsb.org.uk/">anything resembling a blog</a> but I figured with 2016 ringing in a few changes it might be worth giving it a crack again.

![Co Op In Blue For Web1 600x400](../img/co-op-in-blue-for-web1-600x400.jpg)

First up I'm rather excited to be joining the <a href="http://digital.blogs.coop/2015/10/22/digital-talent/">new digital team at the Co-op</a>, helping overhaul their digital presence &amp; services. It's been a while since I worked on site on a regular basis so I'll be working on perfecting my <a href="http://moderntoss.tumblr.com/image/91725132972">water cooler chat</a> too.

As a consequence of this I'll be leaving my home from home for the last couple of years <a href="http://rhythm.digital/">RhythmHQ</a> ;( 

On a positive note this will ease the pressure on the coffee round and should defer upgrading the pot until 2017.

Finally, I've just ported this 'ere website to <a href="http://jekyllrb.com/">Jekyll</a>. I've been using Jekyll for a few recent projects and it's quickly become my software of choice for static sites, prototypes, <a href="https://github.com/opattison/Pattern-Primer-Jekyll">pattern libraries</a> and is even looking interesting <a href="http://cloudcannon.com/">for CMS sites</a>.